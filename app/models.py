from django.db import models
from django.contrib.auth.models import User
from django.core import serializers

def upload_to_categories(instance, filename):

    return 'img/categories/{}/{}'.format(instance.id, filename)

def upload_to_products(instance, filename):

    return 'img/products/{}/{}'.format(instance.id, filename)

class Category(models.Model):

    name      = models.CharField(max_length=254, blank=False)
    image       = models.ImageField(max_length=254, blank=True, null=True, upload_to=upload_to_categories)
    number      = models.IntegerField(default=0)
    units =  models.CharField(max_length=254, blank=True, null=True)


class Product(models.Model):
    category = models.ForeignKey(Category,  related_name= 'product_category',  on_delete=models.SET_NULL, null = True)
    name = models.CharField(max_length=254, blank=False)
    code = models.TextField(max_length=16, blank=True)
    description = models.TextField(blank=True)
    image = models.ImageField(max_length=254, blank=True, null=True, upload_to=upload_to_products)
    price = models.FloatField(blank=False)
    number      = models.IntegerField(default=0)

class ProductOptions(models.Model):
    product = models.ForeignKey(Product,  related_name= 'product_productoptions',  on_delete=models.SET_NULL, null = True)
    quantity = models.IntegerField(default=1)
    comments = models.TextField(blank=True)


class UserProfile(models.Model):
    customerName = models.CharField(max_length=254, null=True)
    address = models.TextField(max_length=16, blank=True)
    customerPhone = models.CharField(max_length=30, null=True)
    email = models.CharField(max_length=100, null=True)
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        default='userprofile'
    )

class Order(models.Model):
    productOptions = models.ManyToManyField(ProductOptions)
    date = models.DateTimeField(auto_now_add=True)
    price = models.FloatField(blank=False, default=0)
    client = models.ForeignKey(UserProfile,  related_name= 'order_client',  on_delete=models.SET_NULL, null = True)
    active = models.BooleanField(default=True)

# types: txt, num, sel, selm
class FormField(models.Model):
    category = models.ForeignKey(Category,  related_name= 'form_category',  on_delete=models.SET_NULL, null = True)
    order = models.IntegerField(default=0)
    field_type = models.CharField(max_length=5, null=True)
    name = models.CharField(max_length=254, blank=False)

class Option(models.Model):
    field = models.ForeignKey(FormField,  related_name= 'option_field',  on_delete=models.SET_NULL, null = True)
    name = models.CharField(max_length=254, blank=False)

class Answer(models.Model):
    productOptions = models.ForeignKey(ProductOptions,  related_name= 'answer_productoptions',  on_delete=models.SET_NULL, null = True)
    field = models.ForeignKey(FormField,  related_name= 'answer_field',  on_delete=models.SET_NULL, null = True)
    answer = models.CharField(max_length=254, blank=False)