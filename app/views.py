from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from app.models import *
from app.serializers import *
from rest_framework  import generics, permissions, response, status, filters
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView
from django.core.mail import EmailMessage
from rest_framework.authentication import SessionAuthentication, BasicAuthentication 
from rest_framework.permissions import BasePermission, IsAuthenticated, IsAdminUser, SAFE_METHODS
from django.core.mail import send_mail

class EmailUtils():


	def new_order(userProfile, order):
		products = ""
		for productOption in order.productOptions.all():
			products = products + productOption.product.name + "\nCantidad: "+str(productOption.quantity)+ "\n\n"

		send_mail(
			'Orden de Compra',
			'Hola, '+ userProfile.customerName +'.\nSe ha registrado la siguiente orden de compra a tu nombre: \n\n'+
			products+'Precio total: \n'+str(order.price),
			'info@sugarliciouspasteleria.com',
			[userProfile.email,"info@sugarliciouspasteleria.com"],
			fail_silently=False,
		)

class Users(generics.ListCreateAPIView):
	queryset = User.objects.all()
	serializer_class = ExtendedUserSerializer

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = User.objects.all()
	serializer_class = ExtendedUserSerializer

class Categories(generics.ListAPIView):
	permission_classes = (permissions.AllowAny,)
	queryset = Category.objects.all().order_by('number')
	serializer_class = CategorySerializer

class EditCategory(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (IsAdminUser,)

class UploadCategoryImage(generics.UpdateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryImageSerializer
    permission_classes = (IsAdminUser,)

class CreateCategory(generics.CreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (IsAdminUser,)

class CategoriesPositioning(APIView):
	permission_classes = (IsAdminUser,)

	def post(self, request, pk, direction):
		try:			
			allcategories = Category.objects.all().order_by('number')
			i = 0
			for item in allcategories:
				item.number = i
				i = i + 1
				item.save()

			category = Category.objects.get(id=pk)
			if direction == "up":
				interchange = Category.objects.get(number=category.number - 1)
				category.number = category.number - 1
				interchange.number = interchange.number + 1

				
			if direction == "down":
				interchange = Category.objects.get(number=category.number + 1)
				category.number = category.number + 1
				interchange.number = interchange.number - 1

			category.save()
			interchange.save()
			return response.Response({'success'}, status=status.HTTP_200_OK)
		except:
			return response.Response({'error'}, status=status.HTTP_400_BAD_REQUEST)


class ProductsPositioning(APIView):
	permission_classes = (IsAdminUser,)

	def post(self, request, pk, direction):
					
		allproducts = Product.objects.all().order_by('number')
		i = 0
		for item in allproducts:
			item.number = i
			i = i + 1
			item.save()

		product = Product.objects.get(id=pk)
		if direction == "up":
			interchange = Product.objects.get(number=product.number - 1)
			product.number = product.number - 1
			interchange.number = interchange.number + 1

			
		if direction == "down":
			interchange = Product.objects.get(number=product.number + 1)
			product.number = product.number + 1
			interchange.number = interchange.number - 1

		product.save()
		interchange.save()
		return response.Response({'success'}, status=status.HTTP_200_OK)
		
class Products(generics.ListAPIView):
	permission_classes = (permissions.AllowAny,)
	serializer_class = ProductSerializer
	filter_backends = (filters.SearchFilter,)
	search_fields = ('name',)

	def get_queryset(self):		
		categoryid = self.kwargs['categoryid']			
		products = Product.objects.filter(category=Category.objects.get(id=categoryid)).order_by('number')
		return products



class EditProduct(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductEditSerializer
    permission_classes = (IsAdminUser,)

class CreateProduct(APIView):
	permission_classes = (IsAdminUser,)

	def post(self, request):
					
		productPost = request.data

		product = Product(category=Category.objects.get(id=productPost['category']), name=productPost['name'], description=productPost['description'],price=productPost['price'])
		product.save()
		return response.Response(ProductSerializer(product).data, status=status.HTTP_200_OK)
		

class UploadProductImage(generics.UpdateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductImageSerializer
    permission_classes = (IsAdminUser,)

class CurrentUserView(generics.RetrieveAPIView):

    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request):
        serializer = ExtendedUserSerializer(request.user)
        return response.Response(serializer.data)
 

class CreateOrder(generics.CreateAPIView):
	
	serializer_class = OrderSerializer

	def post(self,request):		
		
		postProducts = request.data['products']
	
		try:
			up = UserProfile.objects.get(email=request.data['email'])
			up.customerName = request.data['customerName']
			up.address = request.data['address']
			up.customerPhone = request.data['customerPhone']
			up.email = request.data['email']
			up.save()
		except:
			user = User(username=request.data['email'])
			user.save()
			up=UserProfile(user=user, customerName=request.data['customerName'], address=request.data['address'], customerPhone=request.data['customerPhone'], email=request.data['email'])
			up.save()
		order = Order(client=up)
		order.save()

		
		for productOption in postProducts:

			prod = Product.objects.get(id=productOption['product']['id'])
			prodOps = ProductOptions(quantity=int(productOption['quantity']),product=prod)	
			prodOps.save()		
			for answer in productOption['form_answers']:
				Answer(field=FormField.objects.get(id=answer['current_field_id']),answer=answer['answer'],productOptions=prodOps).save()
				
			

			
			order.productOptions.add(prodOps)
			order.price = order.price + (prod.price * prodOps.quantity)
			order.save()

		EmailUtils.new_order(up,order)
		return response.Response(OrderSerializer(order).data, status=status.HTTP_200_OK)
		


class Orders(generics.ListCreateAPIView):
	permission_classes = (permissions.AllowAny,)
	queryset = Order.objects.all().order_by('-date')
	serializer_class = OrderSerializer

class ActiveOrders(generics.ListCreateAPIView):
	permission_classes = (IsAdminUser,)
	queryset = Order.objects.filter(active=True).order_by('-date')
	serializer_class = OrderSerializer

class DeactivateOrder(APIView):
	permission_classes = (permissions.IsAdminUser,)
	serializer_class = OrderSerializer

	def post(self,request,pk):		
		order = Order.objects.get(id=pk)
		order.active=False
		order.save()
		return response.Response({}, status=status.HTTP_200_OK)

class FormFieldsCreation(APIView):
	permission_classes = (IsAdminUser,)

	def post(self, request, category_id):
					
		category = Category.objects.get(id=category_id)
		form_fields = request.data['form_fields']
		for field in FormField.objects.all():
			if field.category==category:
				field.delete()
				Option.objects.filter(field=field).delete()
				
		for form_field in form_fields:
			form_f = FormField(category=category,field_type=form_field['field_type'],name=form_field['name'])
			form_f.save()

			if form_field['field_type']=="sel" or form_field['field_type']=="selm":
				try:
					for option in form_field['options']:
						Option(field=form_f,name=option['name']).save()
				except:
					pass
		return response.Response({'success'}, status=status.HTTP_200_OK)

class FormFields(generics.ListAPIView):
	permission_classes = (permissions.AllowAny,)
	serializer_class = FormFieldSerializer
	def get_queryset(self):
		category_id = self.kwargs['category_id']        
		return FormField.objects.filter(category=Category.objects.get(id=category_id))

class OptionsCreation(APIView):
	permission_classes = (IsAdminUser,)

	def post(self, request, field_id):
					
		field = FormField.objects.get(id=field_id)
		options = request.data['options']
		for option in options:
			op = Option(field=field,name=form_field['name'])
			op.save()
		return response.Response({'success'}, status=status.HTTP_200_OK)

class Options(generics.ListAPIView):
	permission_classes = (permissions.AllowAny,)
	serializer_class = OptionSerializer
	def get_queryset(self):
		field = self.kwargs['field']        
		return FormField.objects.filter(field=FormField.objects.get(id=field))

class CategoryUnits(APIView):
	permission_classes = (IsAdminUser,)

	def post(self, request, category_id):
					
		category = Category.objects.get(id=category_id)
		category.units = request.data['units']
		category.save()
		return response.Response({'success'}, status=status.HTTP_200_OK)

class CategoryUnitsGet(APIView):
	permission_classes = (permissions.AllowAny,)

	def get(self, request, category_id):
					
		category = Category.objects.get(id=category_id)
		return response.Response({category.units}, status=status.HTTP_200_OK)