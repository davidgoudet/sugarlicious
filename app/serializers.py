from django.contrib.auth.models import User, Group
from app.models import *
from rest_framework import serializers
from django.contrib.auth.validators import UnicodeUsernameValidator




class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'

class ProductSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Product
        fields = '__all__'

class FormFieldSerializer(serializers.ModelSerializer):
    options = serializers.SerializerMethodField()

    def get_options(self, formfield):
        return OptionSerializer(Option.objects.filter(field=formfield).order_by('id'), many=True).data

    class Meta:
        model = FormField
        fields = ('id','category','order','field_type','name','options')

class AnswerSerializer(serializers.ModelSerializer):
    field = FormFieldSerializer(read_only=True)
    class Meta:
        model = Answer
        fields = '__all__'

class ProductOptionsSerializer(serializers.ModelSerializer):
    product = ProductSerializer(read_only=True)
    answers = serializers.SerializerMethodField()

    def get_answers(self, productOptions):
        try:
            form_fields = FormField.objects.filter(category=productOptions.product.category)
            answers = []
        
            for form_field in form_fields:
                answers.append(Answer.objects.get(field=form_field,productOptions=productOptions))

            return AnswerSerializer(answers,many=True).data
        except:
            return AnswerSerializer([],many=True).data

    class Meta:
        model = ProductOptions
        fields = '__all__'

class UserProfileSerializer(serializers.ModelSerializer):
    '''
        Serializes profile data for users.
    '''

    class Meta:
        model  =  UserProfile
        fields = '__all__'

class OrderSerializer(serializers.ModelSerializer):
    productOptions = ProductOptionsSerializer(read_only=True, many=True)
    client = UserProfileSerializer(read_only=True)
    date = serializers.DateTimeField(format="%d-%m-%Y %H:%M")
    

    class Meta:
        model = Order
        fields = '__all__'
    
class ProductEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('id','name', 'price', 'description')

class GroupSerializer(serializers.ModelSerializer):
    '''
        Serializes administration group (Role) for users.
    '''
    class Meta:
        model = Group
        fields = ('name',)
        extra_kwargs = {
            'name': {
                'validators': [UnicodeUsernameValidator()],
            }
        }

class CategoryImageSerializer(serializers.ModelSerializer):
    '''
        Updates the profile picture for a certain user.
    '''

    class Meta:
        model  =  Category
        fields = ('image',)

class ProductImageSerializer(serializers.ModelSerializer):
    '''
        Updates the profile picture for a certain user.
    '''

    class Meta:
        model  =  Product
        fields = ('image',)


class ExtendedUserSerializer(serializers.ModelSerializer):
    '''
        Same as SimpleUserSerializer but includes the profile data. This serializer is
        used to retrieve/update/delete new users.
    '''

    groups  = GroupSerializer(many=True)
    userprofile = UserProfileSerializer()

    class Meta:
        model = User
        fields = ('id','username', 'email', 'first_name', 'last_name', 'last_login', 'date_joined', 'is_active', 'groups', 'userprofile')
        read_only_fields = ('groups',)

    def validate_email(self, value):
        exists = User.objects.filter(email = value)
        if exists:
            if exists[0].id != self.initial_data['id']:
                raise serializers.ValidationError("Email address already exists.")
        print(self.initial_data['id'])
        return value

    def validate_username(self, value):
        exists = User.objects.filter(username = value)
        if exists:
            if exists[0].id != self.initial_data['id']:
                raise serializers.ValidationError("Username address already exists.")
        return value

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('userprofile')

        if instance.id != self.initial_data['id']:
            raise serializers.ValidationError("ID provided in request body does not match with ID of requested endpoint. Aborting.")

        # we update the provided data for user
        instance.username   = validated_data.get('username', instance.username)
        instance.email      = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name  = validated_data.get('last_name', instance.last_name)

        instance.save()

        
        return instance

class OptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Option
        fields = '__all__'



