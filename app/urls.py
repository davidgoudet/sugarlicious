from django.conf.urls import url, include
from app import views
from django.contrib.auth.models import User
from app.serializers import *
from rest_framework import routers
from rest_framework import generics
from app.models import *


from django.conf import settings
from django.conf.urls.static import static

app_name = "app"

urlpatterns = [
   	
	
	url(r'^users/$', views.Users.as_view(), name='user-list'),
	url(r'^users/current/$', views.CurrentUserView.as_view(), name='current_user_details'),
	url(r'^user-detail/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='user-detail'),	
	url(r'^categories/$', views.Categories.as_view(), name='categories'),
	url(r'^categories/create/$', views.CreateCategory.as_view(), name='create-category'),
	url(r'^categories/edit/(?P<pk>[0-9]+)/$', views.EditCategory.as_view(), name='edit-category'),
	url(r'^categories/delete/(?P<pk>[0-9]+)/$', views.EditCategory.as_view(), name='delete-category'),
	url(r'^categories/positioning/(?P<pk>[0-9]+)/(?P<direction>[a-z]+)/$', views.CategoriesPositioning.as_view(), name='categories-positioning'),
	url(r'^categories/upload/(?P<pk>[0-9]+)/$', views.UploadCategoryImage.as_view(), name='upload-category-image'),
	url(r'^categories/units/(?P<category_id>[0-9]+)/$', views.CategoryUnits.as_view(), name='category-units'),
	url(r'^categories/units/get/(?P<category_id>[0-9]+)/$', views.CategoryUnitsGet.as_view(), name='category-units-get'),
	url(r'^products/(?P<categoryid>[0-9]+)/$', views.Products.as_view(), name='products'),
	url(r'^products/create/$', views.CreateProduct.as_view(), name='create-product'),
	url(r'^products/edit/(?P<pk>[0-9]+)/$', views.EditProduct.as_view(), name='edit-product'),
	url(r'^products/delete/(?P<pk>[0-9]+)/$', views.EditProduct.as_view(), name='delete-product'),
	url(r'^products/upload/(?P<pk>[0-9]+)/$', views.UploadProductImage.as_view(), name='upload-product-image'),
	url(r'^products/positioning/(?P<pk>[0-9]+)/(?P<direction>[a-z]+)/$', views.ProductsPositioning.as_view(), name='categories-positioning'),
	url(r'^orders/$', views.Orders.as_view(), name='orders'),
	url(r'^orders/create/$', views.CreateOrder.as_view(), name='create-order'),
	url(r'^orders/active/$', views.ActiveOrders.as_view(), name='active-orders'),
	url(r'^orders/deactivate/(?P<pk>[0-9]+)/$', views.DeactivateOrder.as_view(), name='deactivate-order'),
	url(r'^formfields/create/(?P<category_id>[0-9]+)/$', views.FormFieldsCreation.as_view(), name='formfields-creation'),
	url(r'^formfields/list/(?P<category_id>[0-9]+)/$', views.FormFields.as_view(), name='formfields-list'),
	url(r'^options/create/(?P<field_id>[0-9]+)/$', views.OptionsCreation.as_view(), name='option-create'),
	url(r'^options/list/(?P<field_id>[0-9]+)/$', views.Options.as_view(), name='option-list'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)