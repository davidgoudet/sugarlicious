from django.conf.urls          import url, include
from django.conf.urls.static   import static
from django.contrib            import admin
from django.conf               import settings
from django.contrib.auth.views import password_reset, password_reset_done, \
                                      password_reset_confirm, password_reset_complete
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
    # backend urls
    url(r'^', include('app.urls')),
    # django rest-auth urls
    url(r'^auth/', include('rest_auth.urls'))

]

